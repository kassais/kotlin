package lab4.resolve

import lab4.task.*


fun main(args: Array<String>){

    val solarSystem = createSolarSystem();

    val sortedPlanets = nameLengthFilter(solarSystem)

    val planetsMap = listTomap(solarSystem)

    val stringifiedPlanets = listToString(solarSystem)

    println(stringifiedPlanets)
}

fun nameLengthFilter(planets: MutableList<Planet>): List<Planet>{

    return  planets.sortedWith(compareByDescending({it.name.length}));

}

fun listTomap(planets: MutableList<Planet>): Map<String, Int>{

    return planets.associateBy({it.name}, {it.mass})
}

fun listToString(planets: MutableList<Planet>): String{

     return planets.fold(""){acc,planet -> acc + " " + planet.name + " has mass " + planet.mass.toString() + "\n"}

}