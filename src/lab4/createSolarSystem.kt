package lab4.resolve
import lab4.task.*

fun createSolarSystem(): MutableList<Planet>{

    fun addPlanet(quantity: Int,  planets: MutableList<Planet> ): MutableList<Planet>{

        var overalQuantity =  getPLanetNames().size


        if(overalQuantity > 0){

            planets.add(Planet(getPLanetNames()[quantity], getMass()[quantity].toInt()))

            if(quantity < overalQuantity -1 )
            {
                addPlanet(quantity+1, planets)
            }

        }
        return planets
    }

    return addPlanet(0,  arrayListOf())
}