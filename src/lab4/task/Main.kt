package lab4.task

data class Planet(val name: String, val mass: Int);

fun getMass(): List<Double> {
    return  listOf(32868.0, 481068.0, 597600.0, 63345.0,
            187664328.0, 56180376.0, 8605440.0, 10159200.0, 1195.0);

}

fun getPLanetNames(): List<String>{
    return  listOf("Mercury", "Venus", "Earth", "Mars",
            "Jupiter", "Saturn", "Uranus", "Neptune", "Pluto");
}
